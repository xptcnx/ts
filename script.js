window.addEventListener('load', function () {
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    var painting = false;
    var coords = [];
    function startPosition() {
        painting = true;
    }
    function draw(event) {
        var x = event.clientX;
        var y = event.clientY;
        if (!painting)
            return;
        ctx.lineWidth = 5;
        ctx.lineCap = 'round';
        ctx.lineTo(x, y);
        coords.push(x, y);
        ctx.stroke();
    }
    canvas.addEventListener('mousedown', startPosition);
    canvas.addEventListener('mouseup', draw);
});
