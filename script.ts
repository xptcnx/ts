window.addEventListener('load', () => {
  const canvas = <HTMLCanvasElement> document.getElementById('canvas');
  const ctx = canvas.getContext('2d');

  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  let painting: boolean = false;
  const coords: Array<number> = [];

  function startPosition() {
    painting = true;
  }

  function draw(event: MouseEvent): void {
    const x = event.clientX;
    const y = event.clientY;
    
    if (!painting) return;
    ctx.lineWidth = 5;
    ctx.lineCap = 'round';

    ctx.lineTo(x, y);
    coords.push(x, y);
    ctx.stroke();
  }
  canvas.addEventListener('mousedown', startPosition);
  canvas.addEventListener('mouseup', draw);
})